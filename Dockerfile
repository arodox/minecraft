FROM openjdk:18-jdk-alpine3.15

LABEL maintainer "arodox"

# Install tools for server
RUN apk add --no-cache -U \
bash \
curl \
jq

# Expose default ports for server and RCON
EXPOSE 25565 25575

# Mount local files storage Volume for file storage
RUN mkdir -p /minecraft
VOLUME ["/minecraft"]

# Add Start Script
WORKDIR /
COPY ./minecraft_start.sh start.sh

RUN chmod +x /start.sh

WORKDIR /minecraft
CMD [ "/start.sh" ]