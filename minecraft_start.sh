#!/bin/bash
if [ $VERSION == "release" ]; then
    release=$(curl -fsSL https://launchermeta.mojang.com/mc/game/version_manifest.json | jq --raw-output '.latest.release')
    versions=$(curl -fsSL https://launchermeta.mojang.com/mc/game/version_manifest.json | jq --raw-output ".versions[] | select(.id == \"${release}\") | .url")
else
    # Find latest minecraft version
    versions=$(curl -fsSL https://launchermeta.mojang.com/mc/game/version_manifest.json | jq --raw-output '.versions[0].url')
fi
result=$?
# Error if failing to download minecraft
if [ $result != 0 ]; then
    echo "Error failed to contact mojang version manifest please check the URL ${versions}"
    exit 1
fi

# Get the server download link
dl=$(curl -fsSL ${versions} | jq --raw-output '.downloads.server.url')
result=$?

# Fail if no link
if [ $result != 0 ]; then
    echo "Error failed to contact mojang please check the server download link ${dl}"
    exit 1
fi

echo "Acquired download link"

# Change into minecraft directory
cd /minecraft

# Download latest server
curl -fsSL -o /tmp/minecraft_server.jar $dl
result=$?

if [ $result != 0 ]; then
    echo "Error there was an issue downloading the server please check your connection"
    exit 1
else
    echo "Server downloaded"
fi

# Check if minecraft is already there
if [ -f "/minecraft/minecraft_server.jar" ]; then
    
    oldchecksum="minecraft_server.jar"
    newchecksum="/tmp/minecraft_server.jar"

    # Check if same version of server
    if cmp -s "$oldchecksum" "$newchecksum"; then
        echo "No Update :)"
    else
        # Copy new version if minecraft has updated
        cp -f /tmp/minecraft_server.jar minecraft_server.jar
        echo "Minecraft has been updated to the latest version"
    fi
else
    # Copy minecraft if it doesn't already exist
    cp -f /tmp/minecraft_server.jar minecraft_server.jar
fi

# Add ELUA acceptence
if [ ! -f "eula.txt" ]; then
    
    touch eula.txt
    echo "eula=true" > eula.txt
fi


# Launch Minecraft
java -jar minecraft_server.jar nogui