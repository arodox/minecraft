# Minecraft server
Download and run the image, every rebuild will install the latest version(including snapshots).

## Variables
**MINECRAFT_MEMMAX** - Sets maximum memory amount for java
**MINECRAFT_MEMMIN** - Sets minimum memory amount for java

## Data Integrity
If you want to keep your data persistant, mount a volume to /minecraft